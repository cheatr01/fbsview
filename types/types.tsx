export interface UserRecord {
    uid: string;
    displayName: string;
    email: string;
    emailVerified: boolean;
    disabled: boolean;
    customClaims: { [key: string]: any; };
}

export interface UsersResponse {
    users: UserRecord[];
    nextPageToken?: string;
}