import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import {useNavigate} from "react-router-dom";
import {useRef, useState} from "react";
import {UserRecord} from "../types";
import {Toast} from "primereact/toast";
import {handleFirebaseError} from "../utils/firebaseUtils";

const SearchUser = () => {
    const navigate = useNavigate();
    const toast = useRef<Toast>(null);

    let userId: string = "";
    const [email, setEmail] = useState("");
    const [uid, setUid] = useState("");

    const search = () => {
        if (uid != "") {
            fetch('/search/id/' + uid)
                .then((response) => {
                    if (!response.ok) {
                        handleFirebaseError(response, toast);
                    }
                    return response.json()
                })
                .then(data => {
                    navigate(`/user/${(data as UserRecord).uid}`)
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        } else if (email != "") {
            fetch('/search/email/' + email)
                .then((response) => {
                    if (!response.ok) {
                        handleFirebaseError(response, toast);
                    }
                    return response.json()
                })
                .then(data => {
                    navigate(`/user/${(data as UserRecord).uid}`)
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        }

        if (userId != "") {
            navigate(`/user/${userId}`)
        }
    }

    return (
        <div>
            <Toast ref={toast}/>
            <div className="flex flex-column text-xl align-items-center justify-content-center">
                <h2>Search User</h2>
                <InputText id="fbsid" placeholder="FirebaseId" className="m-2"
                           onChange={e => setUid(e.target.value)}/>
                <InputText id="fbsid" placeholder="Email" className="m-2"
                           onChange={e => setEmail(e.target.value)}/>
                <Button label="Search" icon="pi pi-search" className="m-2" onClick={search}/>
            </div>
        </div>
    );
}

export default SearchUser;