import {useParams} from "react-router-dom";
import React, {useEffect, useRef, useState} from "react";
import {Button} from "primereact/button";
import {Panel, PanelHeaderTemplateOptions} from "primereact/panel";
import {Toast} from "primereact/toast";
import {InputTextarea} from "primereact/inputtextarea";

const EditClaims = () => {
    const {userId} = useParams();
    const toast = useRef<Toast>(null);
    const [claimString, setClaimString] = useState("");

    useEffect(() => {
        console.log(userId);
        if (userId != undefined) {
            fetchUser();
        } else {
            console.log("userId is undefined");
        }
    }, []);

    const fetchUser = () => {
        fetch('/user/' + userId)
            .then((response) => response.json())
            .then((data) => {
                const claims = data.customClaims;
                claims ? setClaimString(JSON.stringify(claims, null, 2)) : setClaimString("");
            });
    };

    const updateClaims = () => {
        console.log("Claim String: ", claimString);
        try {
        checkAndParse(claimString);
        }catch (e) {
            console.log(e);
            return;
        }

        fetch(`/user/${userId}/claim`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: claimString,
            }
        )
            .then(() => showSuccess())
            .catch(e => showError(e));
    }

    const beautifyClaims = () => {
        try {
            const claims = checkAndParse(claimString);
            setClaimString(JSON.stringify(claims, null, 2));
        }catch (e) {
            console.log(e);
        }
    }

    const checkAndParse = (claimString: string) => {
        try {
            const claims = JSON.parse(claimString);
            return claims;
        } catch (e) {
            const err = e as SyntaxError
            console.log(e);
            toast.current?.show({
                severity: 'warn',
                summary: `JSON Syntax Error`,
                sticky: true,
                detail: err.message,
            });
            throw err;
        }
    }

    const showSuccess = () => {
        toast.current?.show({severity: 'success', summary: 'Success', detail: 'Claims updated', life: 3000});
    }

    const showError = (response: globalThis.Response) => {
        response.json().then((data) => {
                toast.current?.show({
                    severity: 'error',
                    summary: `Error ${response.status}`,
                    detail: data.error.message,
                    life: 3000
                });
            }
        )
            .catch(() => {
                toast.current?.show({
                    severity: 'error',
                    summary: `Error ${response.status}`,
                    detail: response.statusText,
                    life: 3000
                });
            });
    }

    const headerTemplate = (options: PanelHeaderTemplateOptions) => {
        const className = `${options.className} justify-content-center`;
        const titleClassName = `${options.titleClassName} ml-2 text-primary`;
        const style = {fontSize: '1.5rem'};

        return (
            <div className={className}>
                <span className={titleClassName} style={style}>Edit Claims</span>
            </div>
        );
    };

    return (
        <div>
            <Toast ref={toast}/>
            <div className="flex justify-content-center mt-5">
                <Panel headerTemplate={headerTemplate} className="min-w-max">
                    <InputTextarea value={claimString} onChange={(e) => {
                        setClaimString(e.target.value);
                    }} cols={80} rows={20}
                    />
                    <div className="flex flex-row justify-content-around mt-4">
                        <Button className="max-w-7rem" label="Back" severity="info" icon={"pi pi-arrow-left"}
                                onClick={() => window.history.back()}/>
                        <Button className="max-w-7rem" label="Save" severity="success" icon={"pi pi-save"}
                                onClick={updateClaims}/>
                        <Button className="max-w-7rem" label="Beautify" severity="help" icon={"pi pi-code"}
                                onClick={beautifyClaims}/>
                    </div>
                </Panel>
            </div>
        </div>
    );
}

export default EditClaims;