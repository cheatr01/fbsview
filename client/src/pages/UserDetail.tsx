import {useNavigate, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {UserRecord} from "../types";
import {Panel, PanelHeaderTemplateOptions} from "primereact/panel";
import {Button} from "primereact/button";
import CustomClaims from "../components/CustomClaims";

const UserDetail = () => {
    const navigate = useNavigate();

    const {userId} = useParams();
    const [user, setUser] = useState({} as UserRecord);

    const fetchUser = () => {
        fetch('/user/' + userId)
            .then((response) => response.json())
            .then((data) => {
                setUser(data as UserRecord);
            });
    };

    useEffect(() => {
        console.log(userId);
        if (userId !== undefined) {
            fetchUser();
        } else {
            console.log("userId is undefined");
        }
    }, []);


    const headerTemplate = (options: PanelHeaderTemplateOptions) => {
        const className = `${options.className} justify-content-between`;
        const titleClassName = `${options.titleClassName} ml-2 text-primary`;
        const style = {fontSize: '1.5rem'};

        return (
            <div className={className}>
                <span className={titleClassName} style={style}>Custom Claims</span>
                <Button rounded icon={"pi pi-pencil"}
                        onClick={() => navigate(`/edit/claims/${userId}`)}/>
            </div>
        );
    };

    return (
        <div>
            <Panel header="User Deltails">
                <div className="flex flex-column text-xl">
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"FirebaseId"}</div>
                        <div className="col-10">{user.uid}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Email"}</div>
                        <div className="col-10">{user.email}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"DisplayName"}</div>
                        <div className="col-10">{user.displayName}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Email verified"}</div>
                        <div className="col-10">{user.emailVerified ? "true" : "false"}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Disabled"}</div>
                        <div className="col-10">{user.disabled ? "true" : "false"}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Created at"}</div>
                        <div className="col-10">{user.metadata?.creationTime}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Last logged"}</div>
                        <div className="col-10">{user.metadata?.lastSignInTime}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Last refresh"}</div>
                        <div className="col-10">{user.metadata?.lastRefreshTime}</div>
                    </div>
                </div>
            </Panel>

            <Panel headerTemplate={headerTemplate}>
                <div className={"flex flex-row justify-content-center"}>
                    <CustomClaims customClaims={user.customClaims}/>
                </div>
            </Panel>
        </div>
    );
}

export default UserDetail;