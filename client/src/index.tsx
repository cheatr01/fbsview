import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Users from "./pages/Users";
import UserDetail from "./pages/UserDetail";
import SearchUser from "./pages/Search";
import EditClaims from "./pages/EditClaims";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
      <BrowserRouter>
          <App/>
          <Routes>
              <Route path="/" element={<Users/>}/>
              <Route path="/search" element={<SearchUser/>}/>
              <Route path="/user/:userId" element={<UserDetail/>}/>
              <Route path="/edit/claims/:userId" element={<EditClaims/>}/>
          </Routes>
      </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
