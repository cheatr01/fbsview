import {Toast} from "primereact/toast";

export function handleFirebaseError(response: Response, toast: React.RefObject<Toast>) {
    response.json().then((data) => {
        console.log(`Error data: ${JSON.stringify(data)}`);
        toast.current?.show({
            severity: 'error',
            summary: `Error ${response.status}`,
            detail: data.message,
            life: 3000
        });
        return
    })
        .catch(() => {
            console.log(`Error status: ${response.status}`);
            toast.current?.show({
                severity: 'error',
                summary: `Error ${response.status}`,
                detail: response.statusText,
                life: 3000
            });
            return
        });
}