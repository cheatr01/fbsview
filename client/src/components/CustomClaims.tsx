interface CustomClaimsProps {
    customClaims: object | undefined | null;
}

const CustomClaims = (props: CustomClaimsProps) => {

    return (
            <div>
                <pre>
                    {props.customClaims ? JSON.stringify(props.customClaims, null, 2) : ""}
                </pre>
            </div>
    )
}

export default CustomClaims;