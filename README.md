# Firebase Auth Custom Claim Viewer

This is a simple web app that allows you to view and update the custom claims of a user in your Firebase project.

## Licence
**Creative Commons Non-Commercial** (CC BY-NC). This software is intended to facilitate the work of developers. No developer, their employer, or their client should pay to use this software. 

## Auth to Firebase
For authorization to Firebase is best option to use service account. You can create it in Firebase console. After that you need to download json file with credentials and put it in root folder of this project. Then you need to set environment variable `GOOGLE_APPLICATION_CREDENTIALS` with path to this file.
```shell
export GOOGLE_APPLICATION_CREDENTIALS=*PATH_TO-SERVICE_ACCOUNT_KEY.json*
```
If your project use Tenants (from GCP IdentityPlatform) you need to set environment variable `TENANT_ID` with id of your tenant.
```shell
export TENANT_ID=*YOUR_TENANT_ID*
```
## Run
```shell
```shell
npm install
cd client
npm install
cd ..
npm run start
```

## Usage
You can list users in Firebase, search user by uid and email and simple look on custom claims of each user. In edit page you may edit existing custom claims, delete existing claims or add new one.

