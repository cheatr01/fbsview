const express = require('express');
const {auth} = require("./firebase/firebase");
const {handleFirebaseError} = require("./firebase/firebaseAdminUtils");

const app = express();

app.use(express.json());

app.listen(3001, () => {
    console.log('server running on port 3001');
});

app.get('/users', (req, res) => {
        auth.listUsers(20, req.query.pageToken).then(
            (listUsersResult) => {
                res.send({users: listUsersResult.users, nextPageToken: listUsersResult.pageToken});
            }
        ).catch((error) => {
            handleFirebaseError(error, res);
        });
    }
);

app.get('/user/:userId', (req, res) => {
        auth.getUser(req.params.userId).then(
            (userRecord) => {
                const claims = userRecord.customClaims;
                const claimsString = JSON.stringify(claims, null, 2);
                console.log("Claim string: ", claimsString);
                res.send(userRecord);
            }
        ).catch((error) => {
            handleFirebaseError(error, res);
        });
    }
);

app.get('/search/id/:uid', (req, res) => {
    auth.getUser(req.params.uid)
        .then(
            (userRecord) => {
                res.send(userRecord);
            })
        .catch(
            (error) => {
                handleFirebaseError(error, res);
            }
        )
});

app.get('/search/email/:email', (req, res) => {
    auth.getUserByEmail(req.params.email)
        .then(
            (userRecord) => {
                res.send(userRecord);
            })
        .catch(
            (error) => {
                handleFirebaseError(error, res);
            }
        )
});

app.put('/user/:userId/claim', (req, res) => {
    const claims = req.body;
    console.log(claims);
    auth.setCustomUserClaims(req.params.userId, claims)
        .then(
            (userRecord) => {
                res.send(userRecord);
            })
        .catch(
            (error) => {
                handleFirebaseError(error, res);
            }
        )
});