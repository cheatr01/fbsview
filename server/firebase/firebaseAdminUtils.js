
const handleFirebaseError = (error, response) => {
    switch (error.code) {
        case 'auth/user-not-found':
            console.log('User not found: ' + error);
            response.status(404).send(error);
            break;
        default:
            console.log('Unknown error from Firebase: ', error);
            response.status(500).send(error);
            break;
    }
}

exports.handleFirebaseError = handleFirebaseError;