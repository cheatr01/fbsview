const { initializeApp, applicationDefault} = require('firebase-admin/app');
const {getAuth} = require("firebase-admin/auth");

const tenantId = process.env.TENANT_ID;

const app = initializeApp({
    credential: applicationDefault(),

});

const auth = tenantId ? getAuth(app).tenantManager().authForTenant(tenantId) : getAuth(app)

exports.auth = auth;